package com.training.linkedlist;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.ListIterator;

import org.junit.Test;

import com.training.linkedlist.ListTest;

public class IteratorTest extends ListTest {

	@Test
	public void BasicIteratorTest() {
		list.add(S1);
		list.add(S2);
		list.add(S3);
		list.add(S4);
		list.add(S5);
		list.add(S6);
		list.add(S7);
		list.add(S8);
		list.add(S9_02);
		list.add(S10_07);
		list.add(S11);
		Iterator<String> iterator = list.iterator();
		int i = 0;
		while (iterator.hasNext()) {
			String string = (String) iterator.next();
			assertSame(string,list.get(i));
			i++;
		}
	}
	
	@Test
    public void AdvancedListIterator() {
        String s1 = new String("Pes");
        String s2 = new String("Klobasa");
        String s3 = new String("Parek");
        list.add(s1);
        list.add(s2);
        list.add(s3);
 
        final ListIterator<String> it = list.listIterator();
 
        int index = 0;
        assertFalse(it.hasPrevious());
        assertTrue(it.hasNext());
        assertEquals(-1, it.previousIndex());
        assertEquals(0, it.nextIndex());
        assertSame(s1, it.next());
 
        assertTrue(it.hasPrevious());
        assertTrue(it.hasNext());
        assertEquals(1, it.nextIndex());
        assertEquals(0, it.previousIndex());
        assertEquals(s2, it.next());
 
        assertTrue(it.hasPrevious());
        assertTrue(it.hasNext());
        assertEquals(2, it.nextIndex());
        assertEquals(1, it.previousIndex());
        assertEquals(s3, it.next());
 
        assertTrue(it.hasPrevious());
        assertFalse(it.hasNext());
        assertEquals(2, it.previousIndex());
    }
 
	@Test
    public void iteratorAdd() {        
        
        list.add(S1);
        list.add(S2);
        list.add(S3);
        list.add(S4);
 
        final ListIterator<String> it = list.listIterator();
        it.next();
        it.next();
 
        assertEquals(1, it.previousIndex());
        assertEquals(2, it.nextIndex());
        assertEquals(4, list.size());
        assertSame(S2, it.previous());
        it.next();
        it.add(S5);
        assertSame(S5, it.previous());
 
    }
	
}
