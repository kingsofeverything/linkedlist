package com.training.linkedlist;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Daniel Zvir on 21.05.2016.
 */
public class ContainsListTest extends ListTest {
    @Test
    public void containsRemoveContains() {
        list.add(String.valueOf(50));
        assertTrue(list.contains(String.valueOf(50)));

        assertTrue(list.remove(String.valueOf(50)));

        assertFalse(list.contains(String.valueOf(50)));
    }
}
