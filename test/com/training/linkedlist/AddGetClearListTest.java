package com.training.linkedlist;

import static org.junit.Assert.*;

import org.junit.Test;

import com.training.linkedlist.ListTest;
 
public class AddGetClearListTest extends ListTest {

   

	@Test
    public void basicAdd() {
        assertEquals(0, list.size());
        assertTrue(list.isEmpty());

        list.add("Hello");
        assertEquals(1, list.size());
        assertFalse(list.isEmpty());

        list.add("Nazdar");
        assertEquals(2, list.size());
        assertFalse(list.isEmpty());
    }

    @Test
    public void bulkAdd() {
        for (int i = 0; i < BIG_LIST_SIZE; i++) {
            list.add(String.valueOf(i));
        }
        assertEquals(BIG_LIST_SIZE, list.size());

        assertEquals(String.valueOf(50), list.get(50));
        list.clear();
        assertEquals(0, list.size());
        assertTrue(list.isEmpty());
    }

    @Test
    public void clearEmptyList() {
        assertEquals(0, list.size());
        assertTrue(list.isEmpty());

        list.clear();
        assertEquals(0, list.size());
        assertTrue(list.isEmpty());

        list.clear();
        assertEquals(0, list.size());
        assertTrue(list.isEmpty());
    }

    @Test
    public void addGet() {
        final String item0 = "Hello";
        list.add(item0);
        assertEquals(1, list.size());
        assertFalse(list.isEmpty());

        final String item1 = "Nazdar";
        list.add(item1);
        assertEquals(2, list.size());
        assertFalse(list.isEmpty());

        final String actual0 = list.get(0);
        final String actual1 = list.get(1);
        assertSame(item0, actual0);
        assertSame(item1, actual1);
    }

    @Test
    public void toArray() {
        for (int i = 0; i < BIG_LIST_SIZE; i++) {
            list.add(String.valueOf(i));
        }

        String arr[] = new String[BIG_LIST_SIZE];
        list.toArray(arr);
        assertEquals(String.valueOf(50), arr[50]);
    }

    @Test
    public void addDuplicateValues() {
        list.add(S1);
        list.add(S2);
        list.add(S3);
        list.add(S4);
        list.add(S5);
        list.add(S6);
        list.add(S7);
        list.add(S8);
        list.add(S9_02);
        list.add(S10_07);
        

        assertEquals(10, list.size());
        assertSame(S3, list.get(2));
        assertSame(S9_02, list.get(8));
    }

    @Test
    public void addAcceptNullValues() {
        list.add(S1);
        list.add(null);
        list.add(S2);
        list.add(S3);
        list.add(S4);
        list.add(null);
        list.add(S5);
        list.add(S6);
        list.add(S7);
        list.add(S8);
        list.add(S9_02);
        list.add(S10_07);
        
        list.add(null);

        assertEquals(13, list.size());
        assertSame(S7, list.get(8));
        assertSame(null, list.get(1));
        assertSame(null, list.get(15));
    }

    @Test
    public void advancedAdd() {
        for (int i = 0; i < 10; i++) {
            list.add(String.valueOf(i));
        }
        list.add(5, "Pes");
        assertEquals("Pes", list.get(5));
        assertEquals("5", list.get(6));
        assertEquals("9", list.get(10));
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void getTestOutOfBoundsException() {
        list.add(String.valueOf(50));
        list.get(1);

        list.get(40);
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void addTestOutOfBoundsException() {
        for (int i = 0; i < 50; i++) {
            list.add(String.valueOf(i));
        }
        list.add(51, "Pes");
    }
}

/*
* Napsat testy na vsechno krome metody hash code, spliterator, sublist?
* podivat se na iterator*/