package com.training.linkedlist;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class BasicListTest extends ListTest {

	@Test
	public void isEmptyTest() {
		assertTrue(list.isEmpty());

		list.add(S2);
		assertEquals(1, list.size());
		assertFalse(list.isEmpty());

	}

	@Test
	public void containsTest() {
		String hello = "Hello";
		String hi = "Hi";
		list.add(hello);
		list.add("ahoj");
		String nullString = null;
		assertTrue(list.contains(hello));
		assertFalse(list.contains(hi));
		assertFalse(list.contains(nullString));
	}

	@Test
	public void addTest() {
		assertTrue(list.add("A"));
		assertEquals(1, list.size());
		assertTrue(list.add(null));
		assertEquals(2, list.size());
		assertTrue(list.add(" "));
	}

	@Test
	public void removeTest() {
		assertFalse(list.remove(null));
		assertFalse(list.remove(""));
		String hello = "Hello";
		list.add(hello);
		assertTrue(list.remove(hello));

	}

	@Test
	public void addAllTest() {
		List<String> collection;
		collection = new ArrayList<>();
		collection.add("���j�");
		assertTrue(list.addAll(collection));
	}

	@Test
	public void clearAndSizeTest() {
		final int BIG_LIST_SIZE = 10_000;
		for (int i = 0; i < BIG_LIST_SIZE; i++) {
			list.add(String.valueOf(i));

		}
		assertEquals(BIG_LIST_SIZE, list.size());

		list.clear();
		assertEquals(0, list.size());
	}

	@Test
	public void removeAllTest() {
		List<String> collection;
		collection = new ArrayList<>();
		collection.add("wtf");
		String hello = "Hello";
		list.add(hello);
		assertFalse(list.removeAll(collection));
		collection.add(hello);
		assertTrue(list.removeAll(collection));

	}

	@Test
	public void retainAllTest() {
		List<String> collection;
		collection = new ArrayList<>();
		collection.add("���j�");
		list.add("hello");
		assertTrue(list.retainAll(collection));
		collection.add("hello");
		assertFalse(list.retainAll(collection));

	}

	@Test
	public void GetTest() {
		list.add(S1);
		list.add(S2);
		list.add(S3);
		list.add(S4);
		list.add(S5);
		list.add(S6);
		list.add(S7);
		list.add(S8);
		list.add(S9_02);
		list.add(S10_07);
		list.add(S11);
		assertEquals("David", list.get(0));
		assertEquals("Vaclav", list.get(6));
		assertNotEquals("Dimitri", list.get(6));
	}

	@Test
	public void AddDuplicateValuesTest() {
		list.add(S1);
		list.add(S2);
		list.add(S3);
		list.add(S4);
		list.add(S5);
		list.add(S6);
		list.add(S7);
		list.add(S8);
		list.add(S9_02);
		list.add(S10_07);
		list.add(S11);
		assertEquals(11, list.size());
		assertEquals(S2, list.get(1));
		assertEquals(S7, list.get(6));

	}

	@Test
	public void NullValuesTest() {
		list.add(S1);
		list.add(null);
		list.add(S3);
		assertEquals(null, list.get(1));
		assertEquals(S3, list.get(2));

	}
	
	@Test
	public void removeTest1(){
		list.add(S1);
		list.add(S2);
		list.add(S3);
		list.add(S4);
		list.add(S5);
		list.add(S6);
		list.add(S7);
		list.add(S8);
		list.add(S9_02);
		list.add(S10_07);
		list.add(S11);
		assertEquals(11,list.size());
		list.removeAll(list);		
		assertEquals(0, list.size());
	}
	
	@Test
	public void listContainsMulti(){
		list.add(S1);
		list.add(S2);
		list.add(S3);
		list.add(S4);
		list.add(S5);
		list.add(S6);
		list.add(S7);
		list.add(S8);
		list.add(S9_02);
		list.add(S10_07);
		list.add(S11);
		assertTrue(list.contains("Filip"));
		assertFalse(list.contains("Alah"));
	}
	
	@Test
	public void clearTest(){
		assertEquals(0, list.size());
		list.add(S1);
		list.add(S2);
		list.add(S3);
		list.add(S4);
		list.add(S5);
		list.add(S6);
		list.add(S7);
		list.add(S8);
		list.add(S9_02);
		list.add(S10_07);
		list.add(S11);
		assertEquals(11, list.size());
		list.clear();
		assertEquals(0, list.size());
	}

}