package com.training.linkedlist;

import org.junit.Test;

import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

/**
 * Created by Daniel Zvir on 24.05.2016.
 */
public class SublistTest extends ListTest {
    public void initNames() {
        list.add(S1); // 0
        list.add(S2); // 1
        list.add(S3); // 2
        list.add(S4); // 3
        list.add(S5); // 4
        list.add(S6); // 5
        list.add(S7); // 6
        list.add(S8); // 7
        list.add(S9_02); // 8
        list.add(S10_07); // 9
        
    }

    @Test
    public void AddSizeClear() {
        for (int i = 0; i < BIG_LIST_SIZE; i++) {
            list.add(String.valueOf(i*2));
        }

        final List<String> subList = list.subList(50, 100);
        assertEquals(50, subList.size());
        assertEquals(String.valueOf(198), subList.get(49));

        subList.add(String.valueOf(505));
        assertEquals(String.valueOf(505), subList.get(50));

        assertEquals(String.valueOf(505), list.get(100));

        subList.clear();
        assertEquals(String.valueOf(300), list.get(100));
    }

    @Test
    public void RemoveGetSize() {
        initNames();
        final List<String> subList = list.subList(5, 10);

        subList.remove(2);
        assertEquals(4, subList.size());
        assertEquals(S6, subList.get(0));
        assertEquals(S7, subList.get(1));
        assertEquals(S9_02, subList.get(2));
        assertEquals(S10_07, subList.get(3));

        subList.remove(2);
        assertEquals(S1, list.get(0));
        assertEquals(S2, list.get(1));
        assertEquals(S3, list.get(2));
        assertEquals(S4, list.get(3));
        assertEquals(S5, list.get(4));
        assertEquals(S6, list.get(5));
        assertEquals(S7, list.get(6));
        assertEquals(S10_07, list.get(7));
        assertEquals(S11, list.get(8));
    }

    @Test
    public void ClearListGet() {
        initNames();
        final List<String> subList = list.subList(5, 10);

        subList.clear();
        assertEquals(6, list.size());
        assertEquals(S6, list.get(5));
        assertEquals(S1, list.get(0));
        assertEquals(S4, list.get(3));
    }

    @Test
    public void IteratorSublist() {
        initNames();
        final List<String> subList = list.subList(5, 10);
        //it = subList.listIterator();

        // TODO: 24.05.2016 Create ListIterator test for subList
    }

    @Test (expected = NoSuchElementException.class)
    public void OutOfBoundsExceptionTest() {
        String s1 = new String("Pes");
        String s2 = new String("Klobasa");
        String s3 = new String("Parek");
        list.add(s1);
        list.add(s2);
        list.add(s3);

        final List<String> subList = list.subList(2, 3);
        final ListIterator<String> it = subList.listIterator();
        assertSame(s1, it.previous());
    }
}

