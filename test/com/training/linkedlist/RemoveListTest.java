package com.training.linkedlist;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Created by Daniel Zvir on 20.05.2016.
 */
public class RemoveListTest extends ListTest {

    public void removeOutOfRange() {
        list.add(S1);
        list.add(S2);
        list.add(S3);

        list.remove(3); // Because indexing begins at zero, there is no value at index 3
    }

    @Test
    public void removeData() {
        for (int i = 0; i < BIG_LIST_SIZE; i++) {
            list.add(String.valueOf(i));
        }
        list.remove(50);

        assertEquals(String.valueOf(51), list.get(50));
        assertEquals(BIG_LIST_SIZE - 1, list.size());

        list.remove(50);
        list.remove(50);
        assertEquals(String.valueOf(53), list.get(50));

    }

    @Test
    public void removeNull() {
        for (int i = 0; i < BIG_LIST_SIZE; i++) {
            list.add(String.valueOf(i));
        }

        list.add(0,null);
        assertEquals(null, list.get(0));

        list.remove(0);
        assertEquals(String.valueOf(0), list.get(0));
    }

    @Test
    public void listOfNullValues() {
        for (int i = 0; i < BIG_LIST_SIZE; i++) {
            list.add(null);
        }

        assertSame(null, list.get(500));
    }

    @Test
    public void removeObject() {
        list.add(S1);
        list.add(S2);
        list.add(S3);

        // Object is still present
        assertTrue(list.remove(S3));

        // The object has been removed, and cant be removed again
        assertFalse(list.remove(S3));

        assertFalse(list.remove(S8  ));
    }
}
// dodelat contains!